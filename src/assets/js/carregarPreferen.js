import $ from 'jquery'
import Storage from './storage'
import ReqAPIImages from './reqAPIImages'

// Esta função implementa na pagina após o seu carregamento as 
// preferências salvas pelo usuário.

(function carregarPreferen() {
    $(window).ready(() => {
        const dados = JSON.parse(new Storage().recuperar('obj'))
        if (dados) {
            if (dados.font) $('body').css('font-family', `'${dados.font}'`)

            if (dados.color) {
                $('main').addClass(`${dados.color}`)
            }

            if (dados.pesquisa) {
                $('#inputPesquisa').val(dados.pesquisa)
                let url = `https://dog.ceo/api/breed/${dados.pesquisa}/images`
                new ReqAPIImages().req(url, dados.pesquisa)
            }
            if (dados.select) {
                $('.selecionado-racas').children().first().html(dados.select)
                let url = `https://dog.ceo/api/breed/${dados.select}/images`
                new ReqAPIImages().req(url, dados.select)
            }
        }
    })
})()
import $ from 'jquery'
import RacasSelect from './racasSelect'

// Esta função construtora utiliza a api fetch() para realizar uma requisição assíncrona à
// API, a fim de receber as raças de cães que serão inseridas no select.

function ReqAPI() {
    this.req = () => {
        const url = 'https://dog.ceo/api/breeds/list/all'

        fetch(url)
            .then(data => data.json())
            .then(objs => {
                new RacasSelect().preenchSelect(objs)
            })
            .catch(error => {
                return error
            })
    }
}

new ReqAPI().req()

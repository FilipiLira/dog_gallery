import CarregarImagens from './carregarImagens'
import Message from './mensagem'

// Esta função construtora utiliza a api fetch() para realizar uma requisição assíncrona à
// API, a fim de receber as url correspondentes as imagens.

function ReqAPIImages(){
    this.req = (url, raca) => {

        fetch(url)
            .then(data => data.json())
            .then(objs => {
                objs.status == 'error' ? new Message().error() : new CarregarImagens().carregar(objs, raca)
            })
            .catch(error => {
                return error
            })
    }
}

export default ReqAPIImages
import $ from 'jquery'
import ElemDOM from './ElemDOM'
import MudarFonte from './mudarFonte'
import ReqAPIImages from './reqAPIImages'

// Esta função construtora serve para trocar o valor do select
// de acordo com a opção selecionada, além de instanciar o objeto 
// responsável por fazer a requisição para que os dados sejam retornados.

function RacasSelect() {
    this.preenchSelect = (data) => {
        const racas = Object.keys(data.message)

        racas.forEach(elem => {
            let li = new ElemDOM().li().addClass('raca-li')
            $('#lista-selecionavel-racas').append(li.html(elem))
        });


        let selecionadoRaca = $('.selecionado-racas').children().first()
        let selecionadoR = $('.selecionado-racas')

        $(".raca-li").each((i, li) => {
            $(li).on('click', (e) => {
                let raca = new MudarFonte().mudar(li, selecionadoRaca)
                selecionadoR.attr('value', `${raca}`)

                let url = `https://dog.ceo/api/breed/${raca}/images`

                new ReqAPIImages().req(url, raca)

                $('#inputPesquisa').val('') // limpando valor do input
            })
        })
    }
}

new RacasSelect() 

export default RacasSelect
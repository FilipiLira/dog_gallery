import $ from 'jquery'


function MudarCor() {
    this.mudar = (li, p) =>{
       let liText = li.textContent
       p.html(liText)
       $('body').css('font-family', `'${liText}'`)
    }
}

let selecionadoP = $('.selecionado').children().first()

$(".color-li").each((i, li)=>{
    $(li).on('click', (e)=>{
        new MudarCor().mudar(li, selecionadoP)
    })
})


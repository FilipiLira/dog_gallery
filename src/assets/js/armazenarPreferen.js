import $ from 'jquery'
import Storage from './storage'
import Mensagem from './mensagem'

// Esta função construtora coleta os dados selecionados pelo usuário 
// e passa os dados a um objeto que armazena os dados.

const guardar = new Storage()
function ArmazenarPreferen() {
    $('.btn-salv').on('click', (e) => {

        const fontText =  $('.selecionado-font').attr('value')
        const colorText =  $('.selecionado-color').attr('value')
        const inputPesquisa = $('#inputPesquisa').val().trim().toLowerCase()
        const selectRacas = $('.selecionado-racas').attr('value')
        const dataHoje = new Date().toLocaleDateString()
        const horaAgora = new Date().toLocaleTimeString()
        

        let preferencias = {
            font : fontText,
            color: colorText,
            pesquisa: inputPesquisa,
            select: selectRacas,
            data: dataHoje,
            hora: horaAgora
        }

        let salvou = true
        !preferencias.font && !preferencias.color ? salvou = false : guardar.armazenar(preferencias)

        salvou ? new Mensagem().success() : ''

    })
}

new ArmazenarPreferen()
import $ from 'jquery'
import ElemDOM from './ElemDOM'

// Esta função construtora carrega na DOM os elementos
// com a classe card-img que contém as tags img com src
// obtidos da resposta da API.

function CarregarImagens(){
    this.carregar = (data, raca) => {

        $('.conteudo').html('')

        let p = new ElemDOM().p().html(raca).css('width', '100%').addClass('text-center').css('font-size', '2rem')
        $('.conteudo').append(p)
        data.message.forEach(elem => {
            let img = new ElemDOM().img()
            let div = new ElemDOM().div().addClass('card-img')
            

            img.attr('src', elem).addClass('img-fluid')
            div.append(img)

            $('.conteudo').append(div)
        });
    }
}

export default CarregarImagens
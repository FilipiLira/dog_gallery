import $ from 'jquery'
import ReqAPIImages from './reqAPIImages'

// Esta função serve para realizar uma requisição GET à API,
// de acordo com o conteúdo do input de pesquisa, no momento do 
// clique do botão com icone de lupa. 

(function selecionarPesquisar(){
    $('#btn-pesquisa').on('click',()=>{
        let valorInput = $('#inputPesquisa').val()

        let valorInputPesquisa = valorInput.trim()
        let pesquisa = valorInputPesquisa.toLowerCase()

        let url = `https://dog.ceo/api/breed/${pesquisa}/images`
        new ReqAPIImages().req(url, pesquisa)

        $('.selecionado-racas').attr('value', '') // limpando valor do select
    })
})()
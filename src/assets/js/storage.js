import $ from 'jquery'

// Esta função construtora faz o armazenamento dos dados salvos pelo usuário 
// em window.localStorage.

function Storage(){
    this.armazenar = (preferencias)=>{
        let dados = JSON.stringify(preferencias)
        window.localStorage.setItem('obj', dados)
    }

    this.recuperar = (chave)=>{
        return window.localStorage.getItem(chave)
    }
}

export default Storage
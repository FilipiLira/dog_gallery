import $ from 'jquery'

// Esta função realiza a animação dos selects.

(function trocarIcone(btn) {

    $('.select').each((i, elem) => {
        $(elem).on('click', (e)=>{
            const lista = $(elem).children().last()
            const selecionado = $(elem).children().first()
            const iconeSeta = selecionado.children().last()
            
            if(!$(elem).hasClass('clicado')){
                $(elem).hasClass('select-font') ? lista.css('height', `190px`).css('trasition', 'all .4s ease-in-out') : lista.css('height', `230px`).css('trasition', 'all .4s ease-in-out')
                iconeSeta.css('transform', 'rotate(270deg)')
            } else {
                lista.css('height', `0`)
                iconeSeta.css('transform', 'rotate(-270deg)')
            }

            $(elem).hasClass('clicado') ? $(elem).removeClass('clicado') : $(elem).addClass('clicado')
        })
    })

})()
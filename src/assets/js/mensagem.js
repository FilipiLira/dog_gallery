import $ from 'jquery'
import ElemDOM from './ElemDOM'

// Esta função construtora serve para criar elementos DOM 
// para serem usados em outros modulos do projeto.

function Mensagem() {
    this.success = () => {
        $('#mensagem').css('width', '165px')
        $('.btn-salv').attr('disabled', '')

        setTimeout(() => {
            $('#mensagem').css('width', '0')
            $('.btn-salv').removeAttr('disabled')
        }, 3000)
    }

    this.error = () => {
        let p = new ElemDOM().p().html('Ocorreu um erro. Nenhum resultado retornado').css('width', '100%').addClass('text-center').css('font-size', '2rem')
        $('.conteudo').html(p)
    }
}

export default Mensagem
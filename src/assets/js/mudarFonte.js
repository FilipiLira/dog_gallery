import $ from 'jquery'

// Esta função serve para mudar as cores e fontes de a cordo com as selecionadas.

function MudarFonte() {
    this.anterior = ''
    this.mudar = (li, p) => {
        if ($(li).hasClass('font-li')) {
            let liText = li.textContent
            p.html(liText)
            $('body').css('font-family', `'${liText}'`)

            return liText

        } else if ($(li).hasClass('raca-li')) {
            let liText = li.textContent
            p.html(liText)
            return liText

        } else {
            let color = $(li).children().last().html()
            p.html(color)

            let classAtual = $('main').attr('class').split(' ')[1]
            if(classAtual) $('main').removeClass(`${classAtual}`)

            $('main').removeClass(`${this.anterior}`)
            $('main').addClass(`${color}`)
            this.anterior = color

            return color
        }
    }
}

let selecionadoFont = $('.selecionado-font').children().first()
let selecionadoF = $('.selecionado-font')

const mudarCorFont = new MudarFonte()

$(".font-li").each((i, li) => {
    $(li).on('click', (e) => {
        let font = mudarCorFont.mudar(li, selecionadoFont)
        selecionadoF.attr('value', `${font}`)
    })
})

let selecionadoColor = $('.selecionado-color').children().first()
let selecionadoC = $('.selecionado-color')

$(".color-li").each((i, li) => {
    $(li).on('click', (e) => {
        let color = mudarCorFont.mudar(li, selecionadoColor)
        selecionadoC.attr('value', `${color}`)
    })
})

export default MudarFonte